package entidades;

import java.io.Serializable;
import javax.persistence.*;

@Embeddable
public class Endereco implements Serializable{
    private String rua;
    private String numero;
    private String cidade;
    private String bairro;
   

    public Endereco() {
    }

    public Endereco(String rua, String numero, String cidade, String bairro) {
        this.rua = rua;
        this.numero = numero;
        this.cidade = cidade;
        this.bairro = bairro;
    }

    public String getRua() {
        return rua;
    }

    public void setRua(String rua) {
        this.rua = rua;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    @Override
    public String toString() {
        return "Endereco:" + "rua - " + rua + ", numero - " + numero + ", cidade - " + cidade + ", bairro - " + bairro + '.';
    }
    
}

