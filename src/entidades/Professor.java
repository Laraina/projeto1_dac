package entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class Professor extends Cliente implements Serializable{
   
    @Column(unique = true)
    private String matSiape;
    private String formacao;
    private String email;
    private List<String> disciplinas;

    public Professor() {
    }

    public Professor(String cpf, String nome, String telefones, String matSiape, String formacao, String email, List<String> disciplinas) {
        super(cpf, nome, telefones);
        this.matSiape = matSiape;
        this.formacao = formacao;
        this.email = email;
        this.disciplinas = disciplinas;
    }

    public String getMatSiape() {
        return matSiape;
    }

    public void setMatSiape(String matSiape) {
        this.matSiape = matSiape;
    }

    public String getFormacao() {
        return formacao;
    }

    public void setFormacao(String formacao) {
        this.formacao = formacao;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<String> getDisciplinas() {
        return disciplinas;
    }

    public void setDisciplinas(List<String> disciplinas) {
        this.disciplinas = disciplinas;
    }

    @Override
    public String toString() {
        return "Professor: "+ super.toString() + ", matSiape - " + matSiape + ", \n formacao - " + formacao + ", email - " + email + ", disciplinas - " + disciplinas + '.'+"\n";
    }
    
    
}
