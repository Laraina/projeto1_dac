package entidades;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Entity
public class AtendimentoPedagogico implements Serializable{
    @Id
    @GeneratedValue
    private int codigo;
    
    private String motivo;
    private String encaminhamento;
    
    @Temporal(value=TemporalType.TIMESTAMP)
    private Date data;
    private String dia;
    
    @ManyToOne
    private Funcionario responsavel;
    
    @ManyToOne
    private Cliente atendido;

    public AtendimentoPedagogico() {
    }

    public AtendimentoPedagogico(String motivo, String encaminhamento, Date dataAt, String dia, Funcionario responsavel, Cliente atendido) {
        this.motivo = motivo;
        this.encaminhamento = encaminhamento;
        this.data = dataAt;
        this.dia = dia;
        this.responsavel = responsavel;
        this.atendido = atendido;
    }

   

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public String getEncaminhamento() {
        return encaminhamento;
    }

    public void setEncaminhamento(String encaminhamento) {
        this.encaminhamento = encaminhamento;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data= data;
    }

    public String getDia() {
        return dia;
    }

    public void setDia(String dia) {
        this.dia = dia;
    }
    
    

    public Funcionario getResponsavel() {
        return responsavel;
    }

    public void setResponsavel(Funcionario responsavel) {
        this.responsavel = responsavel;
    }

    public Cliente getAtendido() {
        return atendido;
    }

    public void setAtendido(Cliente atendido) {
        this.atendido = atendido;
    }

    @Override
    public String toString() {
        return "________________________________________________________________________\nAtendimento: " + "Código - " + codigo + ", Motivo - " + motivo + ", \n Encaminhamento - " + encaminhamento + ", \n Data - " + dia + " \n Responsavel pelo atendimento\n" + responsavel + " Cliente atendido\n" + atendido + '.';
    }
    
    
    
    
    
    
    
    

    
}
