package entidades;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;

@Entity
@IdClass(value=FuncionarioPK.class)
public class Funcionario implements Serializable{
    @Id
    private String matricula;
    @Id
    private String nome;
    private String email;
    private String senha;
    private String telefone;
    private String cargo;
    private boolean ativo;
    @OneToMany(mappedBy="responsavel", cascade = {CascadeType.ALL})
    private List<AtendimentoPedagogico> atendimentos; 
    

    public Funcionario() {
    }

    public Funcionario(String matricula, String nome, String email, String senha, String telefone, String cargo) {
        this.matricula = matricula;
        this.nome = nome;
        this.email = email;
        this.senha = senha;
        this.telefone = telefone;
        this.cargo = cargo;
        this.ativo = true;
        this.atendimentos = new ArrayList<>();
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }
    
    

    public List<AtendimentoPedagogico> getAtendimentos() {
        return atendimentos;
    }

    public void setAtendimentos(List<AtendimentoPedagogico> atendimentos) {
        this.atendimentos = atendimentos;
    }

    @Override
    public String toString() {
        return "Funcionario: " + "matricula - " + matricula + ", nome - " + nome + ", email - " + email + ", \n telefone - " + telefone + ", cargo - " + cargo +  '.'+"\n";
    }
    
}
