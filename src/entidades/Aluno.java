package entidades;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Aluno extends Cliente implements Serializable{
    
    @Column(unique = true)
    private String matricula;
    private String turma;
    @Temporal(value=TemporalType.DATE)
    private Date dataNascimento;
    private String email;
    
    @Embedded
    @OneToOne(cascade={CascadeType.ALL})
    private Endereco endereco;
    
    @ManyToMany(cascade={CascadeType.ALL})
    private List<Responsavel> responsaveis;
    
    public Aluno() {
    }

    public Aluno(String cpf, String nome, String telefones, String matricula, String turma, Date dataNascimento, String email, Endereco endereco) {
        super(cpf, nome, telefones);
        this.matricula = matricula;
        this.turma = turma;
        this.dataNascimento = dataNascimento;
        this.email = email;
        this.endereco = endereco;
    }
    

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getTurma() {
        return turma;
    }

    public void setTurma(String turma) {
        this.turma = turma;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public List<Responsavel> getResponsaveis() {
        return responsaveis;
    }
    
    public String listaResponsaveis(List<Responsavel> responsaveis) {
        String lista = "";
        for (Responsavel resp : responsaveis) {
            lista+= resp.getCpf()+", ";            
        }
        return lista;
    }


    public void setResponsaveis(List<Responsavel> responsaveis) {
        this.responsaveis = responsaveis;
    }

    @Override
    public String toString() {
        return "Aluno: " + super.toString() + ", matricula - " + matricula + ", \n turma - " + turma + ", email - " + email + ", "
                + "data de nascimento - " + dataNascimento.toGMTString()+ ", \n " + endereco.toString() + ", \n Responsaveis - " + listaResponsaveis(responsaveis) + '.'+" \n";
    }
    
}
