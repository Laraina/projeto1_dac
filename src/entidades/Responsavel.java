package entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;

@Entity
public class Responsavel extends Cliente implements Serializable{
   
    private String profissao;
    private String escolaridade;
    

    public Responsavel() {
    }

    public Responsavel(String cpf, String nome, String telefones, String profissao, String escolaridade ) {
        super(cpf, nome, telefones);
        this.profissao = profissao;
        this.escolaridade = escolaridade;
        
    }
    
    public String getProfissao() {
        return profissao;
    }

    public void setProfissao(String profissao) {
        this.profissao = profissao;
    }

    public String getEscolaridade() {
        return escolaridade;
    }

    public void setEscolaridade(String escolaridade) {
        this.escolaridade = escolaridade;
    }

    
    
    public String listaResponsaveis(List<Aluno> responsavelPor) {
        String lista = "";
        for (Aluno aluno : responsavelPor) {
            lista+= " CPF - "+aluno.getCpf()+", Nome - "+aluno.getNome();            
        }
        return lista;
    }

   
    @Override
    public String toString() {
        return "Responsavel: " + super.toString() + ", \n profissao=" + profissao + ", escolaridade=" + escolaridade + '.'+"\n";
    }
    
}
