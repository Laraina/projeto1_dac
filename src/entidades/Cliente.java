package entidades;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;

@Entity
@Inheritance(strategy=InheritanceType.JOINED)
public class Cliente implements Serializable{
    
    @Id
    private String cpf;
    private String nome;
    private boolean ativo;
    private String telefones;
    
    @OneToMany(mappedBy="atendido", cascade = {CascadeType.ALL})
    private List<AtendimentoPedagogico> atendimentos;

    public Cliente() {
    }

    public Cliente(String cpf, String nome, String telefones) {
       this.cpf = cpf;
       this.nome = nome;
       this.ativo = true;
       this.telefones = telefones;
       this.atendimentos = new ArrayList<>();
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

        public String getTelefones() {
        return telefones;
    }

    public void setTelefones(String telefones) {
        this.telefones = telefones;
    }

    public List<AtendimentoPedagogico> getAtendimentos() {
        return atendimentos;
    }

    public void setAtendimentos(List<AtendimentoPedagogico> atendimentos) {
        this.atendimentos = atendimentos;
    }

    @Override
    public String toString() {
        return "nome - " + nome + ", cpf - " + cpf + ", telefones - " + telefones +'.';
    }
}
