package sgcoped;

//import java.io.FileOutputStream;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfWriter;
import entidades.AtendimentoPedagogico;
import java.io.FileOutputStream;
import java.io.IOException;

public class AtendPdf {

    private static int i = 1;

    public void geraPdf(AtendimentoPedagogico ap) {
        //criação do objeto documento 
        Document document = new Document();
        try {

            PdfWriter.getInstance(document, new FileOutputStream("C:\\Users\\Edmar\\Documents\\NetBeansProjects\\Projeto1_Dac\\src\\Pdf\\AtendimentoPdf.pdf"));
            document.open();
            
            Image img = Image.getInstance("i2.jpg");
            img.setAlignment(Element.ALIGN_CENTER);
            img.scaleAbsolute(180, 110);
            Font tit = new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.BOLD);
            Paragraph titulo = new Paragraph("IFPB - Campus Cajazeiras \nFicha de Atendimento Pedagógico \n Dados do atendimento: \n\n\n", tit);
            titulo.setAlignment(Element.ALIGN_CENTER);            

            Paragraph dados6 = new Paragraph("Código: " + ap.getCodigo() + "          Data: " + ap.getDia() + "\n\n");
            Paragraph dados1 = new Paragraph("Funcionário responsável : " + ap.getResponsavel().getNome() + "\n");
            Paragraph dados2 = new Paragraph("Cliente Atendido: \n      Nome -  " + ap.getAtendido().getNome() + "\n      Cpf - " + ap.getAtendido().getCpf()+ "\n\n");
           // Paragraph dados3 = new Paragraph("Data: " + ap.getDia() + "\n\n");
            Paragraph dados4 = new Paragraph("Motivo: " + ap.getMotivo() + "\n\n");
            Paragraph dados5 = new Paragraph("Encaminhamento: " + ap.getEncaminhamento() + "\n\n");
            Paragraph espaco = new Paragraph("\n\n\n\n\n");
            
            Paragraph linha = new Paragraph("__________________________________________________");
            linha.setAlignment(Element.ALIGN_CENTER);
            Paragraph ass = new Paragraph("Assinatura do Cliente\n\n\n");
            ass.setAlignment(Element.ALIGN_CENTER);
            Paragraph linha2 = new Paragraph("__________________________________________________");
            linha2.setAlignment(Element.ALIGN_CENTER);
            Paragraph ass2 = new Paragraph("Assinatura do Funcionário");
            ass2.setAlignment(Element.ALIGN_CENTER);

            document.add(img);
            document.add(titulo);
            document.add(dados6);
            //document.add(dados3);
            document.add(dados2);
            document.add(dados4);
            document.add(dados5);
            document.add(dados1);
            document.add(espaco);
            document.add(linha);
            document.add(ass);
            document.add(linha2);
            document.add(ass2);        
            
        } catch (DocumentException | IOException de) {
            System.err.println(de.getMessage());
        }
        document.close();
    }
    
    public void abrirPdf() throws IOException{        
        Runtime.getRuntime().exec("cmd.exe /C C:\\Users\\Edmar\\Documents\\NetBeansProjects\\Projeto1_Dac\\src\\Pdf\\AtendimentoPdf.pdf");
        }

}
