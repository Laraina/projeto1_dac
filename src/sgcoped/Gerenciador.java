package sgcoped;

import daos.DaoJpa;
import entidades.Aluno;
import entidades.AtendimentoPedagogico;
import entidades.Cliente;
import entidades.Funcionario;
import entidades.Professor;
import entidades.Responsavel;
import java.io.IOException;

public class Gerenciador {

    public static Gerenciador gerenciador;
    private final DaoJpa dao = new DaoJpa();
    private final AtendPdf pdf = new AtendPdf();

    protected Gerenciador() {

    }

    //Singleton
    public static Gerenciador getGerenciador() {
        if (gerenciador == null) {
            gerenciador = new Gerenciador();
        }
        return gerenciador;
    }

    // CRUD Funcionario
    public void cadastrarFuncionario(Funcionario f) {
        dao.cadastrarFuncionario(f);
    }

    public String listarFuncionario() {
        return dao.listarFuncionario();
    }

    public boolean deletarFuncionario(String matricula, String nome) {
        return dao.deletarFuncionario(matricula, nome);

    }

    //CRUD AtendimentoPedagogico
    public void cadastrarAtendimento(AtendimentoPedagogico ap) {
        dao.cadastrarAtendimento(ap);
    }

    public String listarAtendimentos() {
        return dao.listarAtendimentos();
    }

    public boolean deletarAtendimento(int codigo) {
        return dao.deletarAtendimento(codigo);
    }

    //Atendimentos por codigo (para deletar)
    public AtendimentoPedagogico atendimentosPorCodigo(int codigo) {
        return dao.atendimentosPorCodigo(codigo);
    }

    //CRUD Cliente
    public void cadastrarCliente(Cliente cli) {
        dao.cadastrarCliente(cli);
    }

    public String listarCliente(String classe) {
        return dao.listarCliente(classe);
    }

    public Cliente pesquisarCliente(String cpf) {
        return dao.pesquisarCliente(cpf);
    }
    
    public Aluno pesquisarAluno(String cpf, String matricula) {
        return dao.pesquisarAluno(cpf, matricula);
    }
    
    public Professor pesquisarProf(String cpf, String matricula) {
        return dao.pesquisarProf(cpf, matricula);
    }
    
    public Responsavel pesquisarResp(String cpf) {
        return dao.pesquisarResp(cpf);
    }

    public boolean deletarCliente(String cpf) {
        return dao.deletarCliente(cpf);
    }

    //Requisitos funcionais    
    //Login adm
    public boolean loginAdm(String user, String senha) {
        return dao.loginAdm(user, senha);
    }

    //Login funcionario
    public Funcionario loginFuncionario(String matricula, String senha) {
        return dao.loginFuncionario(matricula, senha);
    }

    //Atendimentos por data
    public String atendimentosPorData(String data) {
        return dao.atendimentosPorData(data);
    }

    //Atendimentos por cliente
    public String atendimentosPorCliente(String cpf) {
        return dao.atendimentosPorCliente(cpf);
    }
    
    //Atendimentos por turma
     public String atendimentosPorTurma(String turma) {
        return dao.atendimentosPorTurma(turma);
    }
    
     //Ficha em PDF
     public void gerarPdf(AtendimentoPedagogico ap) throws IOException {
        pdf.geraPdf(ap);
        pdf.abrirPdf();
    }

}
