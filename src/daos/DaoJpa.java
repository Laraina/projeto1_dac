/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package daos;

import entidades.Aluno;
import entidades.AtendimentoPedagogico;
import entidades.Cliente;
import entidades.Funcionario;
import entidades.FuncionarioPK;
import entidades.Professor;
import entidades.Responsavel;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author Laraina
 */
public class DaoJpa {

    EntityManagerFactory emf = Persistence.createEntityManagerFactory("Projeto1_DacPU");
    EntityManager em = emf.createEntityManager();

    // CRUD Funcionario
    public void cadastrarFuncionario(Funcionario f) {
        FuncionarioPK chave = new FuncionarioPK(f.getMatricula(), f.getNome());
        em.getTransaction().begin();
        if (em.find(Funcionario.class, chave) == null) {
            em.persist(f);
        } else {
            em.merge(f);
        }
        em.getTransaction().commit();
    }

    public String listarFuncionario() {
        String funcionarios = "";
        List<Funcionario> lista = em.createQuery("SELECT f FROM Funcionario f where f.ativo = true").getResultList();
        for (Funcionario func : lista) {
            funcionarios += func.toString() + "\n";
        }
        return funcionarios;
    }

    public boolean deletarFuncionario(String matricula, String nome) {
        Query q = em.createQuery("SELECT f FROM Funcionario f where f.matricula = :matricula and f.nome=:nome and f.ativo=true");
        q.setParameter("matricula", matricula);
        q.setParameter("nome", nome);
        List<Funcionario> lista = q.getResultList();
        if (lista.size() > 0) {
            for (Funcionario func : lista) {
                func.setAtivo(false);
                em.getTransaction().begin();
                em.merge(func);
                em.getTransaction().commit();
            }
            return true;
        } else {
            return false;
        }

    }

    //CRUD AtendimentoPedagogico
    public void cadastrarAtendimento(AtendimentoPedagogico ap) {
        em.getTransaction().begin();
        if (em.find(AtendimentoPedagogico.class, ap.getCodigo()) == null) {
            em.persist(ap.getResponsavel());
            em.persist(ap.getAtendido());
            em.persist(ap);
        } else {
            em.persist(ap.getResponsavel());
            em.persist(ap.getAtendido());
            em.merge(ap);
        }
        em.getTransaction().commit();
    }

    public String listarAtendimentos() {
        String atendimentos = "";
        List<AtendimentoPedagogico> lista = em.createQuery("SELECT ap FROM AtendimentoPedagogico ap").getResultList();
        for (AtendimentoPedagogico atendimento : lista) {
            atendimentos += atendimento.toString() + "\n";
        }
        return atendimentos;
    }

    public boolean deletarAtendimento(int codigo) {
        AtendimentoPedagogico atendimento = em.find(AtendimentoPedagogico.class, codigo);
        if (atendimento != null) {
            em.getTransaction().begin();
            em.remove(atendimento);
            em.getTransaction().commit();
            return true;
        } else {
            return false;
        }

    }

    //Atendimentos por codigo (para deletar)
    public AtendimentoPedagogico atendimentosPorCodigo(int codigo) {
        AtendimentoPedagogico ap = null;
        Query q = em.createQuery("SELECT ap FROM AtendimentoPedagogico ap WHERE ap.codigo = :codigo");
        q.setParameter("codigo", codigo);
        List<AtendimentoPedagogico> lista = q.getResultList();
        for (AtendimentoPedagogico at : lista) {
            ap = at;
        }
        return ap;
    }

    //CRUD Cliente
    public void cadastrarCliente(Cliente cli) {
        em.getTransaction().begin();
        if (em.find(Cliente.class, cli.getCpf()) == null) {
            em.persist(cli);
        } else {
            em.merge(cli);
        }
        em.getTransaction().commit();
    }

    public String listarCliente(String classe) {
        String clientes = "";
        List<Cliente> lista = em.createQuery("SELECT cli FROM " + classe + " cli where cli.ativo = TRUE").getResultList();
        for (Cliente cliente : lista) {
            clientes += cliente.toString() + "\n";
        }
        return clientes;
    }

    public Cliente pesquisarCliente(String cpf) {
        Cliente cliente = null;
        Query q = em.createQuery("SELECT cli FROM Cliente cli where cli.cpf =:cpf");
        q.setParameter("cpf", cpf);
        List<Cliente> lista = q.getResultList();
        for (Cliente cli : lista) {
            cliente = cli;
        }
        return cliente;
    }
    
    //verifica conflita nas chaves
    public Aluno pesquisarAluno(String cpf, String matricula) {
        Aluno aluno = null;
        Query q = em.createQuery("SELECT aluno FROM Aluno aluno where aluno.cpf =:cpf or aluno.matricula=:matricula");
        q.setParameter("cpf", cpf);
        q.setParameter("matricula", matricula);
        List<Aluno> lista = q.getResultList();
        for (Aluno cli : lista) {
            aluno = cli;
        }
        return aluno;
    }
    
    public Professor pesquisarProf(String cpf, String matricula) {
        Professor prof = null;
        Query q = em.createQuery("SELECT professor FROM Professor professor where professor.cpf =:cpf or professor.matSiape=:matricula");
        q.setParameter("cpf", cpf);
        q.setParameter("matricula", matricula);
        List<Professor> lista = q.getResultList();
        for (Professor cli : lista) {
            prof = cli;
        }
        return prof;
    }
    
    public Responsavel pesquisarResp(String cpf) {
        Responsavel resp = null;
        Query q = em.createQuery("SELECT responsavel FROM Responsavel responsavel where responsavel.cpf =:cpf");
        q.setParameter("cpf", cpf);
        List<Responsavel> lista = q.getResultList();
        for (Responsavel cli : lista) {
            resp = cli;
        }
        return resp;
    }
    
    
     public boolean deletarCliente(String cpf) {
        Query q = em.createQuery("SELECT cli FROM Cliente cli where cli.cpf = :cpf and cli.ativo=true");
        q.setParameter("cpf", cpf);
        List<Cliente> lista = q.getResultList();
        if (lista.size() > 0) {
            for (Cliente cliente : lista) {
                cliente.setAtivo(false);
                em.getTransaction().begin();
                em.merge(cliente);
                em.getTransaction().commit();
            }
            return true;
        } else {
            return false;
        }
    }

    //Requisitos funcionais    
    //Login adm
    public boolean loginAdm(String user, String senha) {
        boolean liberar = false;
        if ((user.equalsIgnoreCase("admin")) && (senha.equalsIgnoreCase("admin"))) {
            liberar = true;
        }
        return liberar;
    }

    //Login funcionario
    public Funcionario loginFuncionario(String matricula, String senha) {
        Funcionario atual = null;
        Query q = em.createQuery("SELECT f FROM Funcionario f WHERE f.matricula = :matricula and f.senha=:senha and f.ativo=true");
        q.setParameter("matricula", matricula);
        q.setParameter("senha", senha);
        List<Funcionario> funcs = q.getResultList();
        for (Funcionario func : funcs) {
            atual = func;
        }
        return atual;
    }

    //Atendimentos por data
    public String atendimentosPorData(String data) {
        String atendimentos = "";
        Query q = em.createQuery("SELECT ap FROM AtendimentoPedagogico ap WHERE ap.dia =:data");
        q.setParameter("data", data);
        List<AtendimentoPedagogico> lista = q.getResultList();
        for (AtendimentoPedagogico at : lista) {
            atendimentos += at.toString();
        }
        return atendimentos;
    }

    //Atendimentos por cliente
    public String atendimentosPorCliente(String cpf) {
        String atendimentos = "";
        Query q = em.createQuery("SELECT ap FROM AtendimentoPedagogico ap LEFT JOIN ap.atendido at WHERE at.cpf = :cpf");
        q.setParameter("cpf", cpf);
        List<AtendimentoPedagogico> lista = q.getResultList();
        for (AtendimentoPedagogico at : lista) {
            atendimentos += at.toString();
        }
        return atendimentos;
    }
    
     public String atendimentosPorTurma(String turma) {
        String atendimentos = "";        
        Query q = em.createQuery("SELECT ap FROM AtendimentoPedagogico ap WHERE ap.atendido=(SELECT a FROM Aluno a WHERE a.turma=:turma)");
        q.setParameter("turma", turma);
        List<AtendimentoPedagogico> lista = q.getResultList();
        for (AtendimentoPedagogico at : lista) {
            atendimentos += at.toString();
        }
        return atendimentos;
    }
     
   

}
